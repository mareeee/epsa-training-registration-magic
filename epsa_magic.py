import sys
import csv
import functools
import os

# file reading
if len(sys.argv) < 2:
    filename = input('Type the name of the input file:\n')
    slot_input = input('How many participants can attend a training?\n')
else:
    filename = sys.argv[1]
    if len(sys.argv) > 2:
        slot_input = sys.argv[2]
    else:
        slot_input = ''
try:
    csv_file = open(filename, 'r', encoding='utf8')
except FileNotFoundError:
    print(f"Cannot find file \"{filename}\"")
    sys.exit()

try:
    slot_limit = int(slot_input)
except ValueError:
    slot_limit = 25
    print(f'Number of participants per training is set to the default value: {25}')

email_index = 2
training_index = 6
choice_count = 1

registered_users = list()
data = dict()

csv_reader = csv.reader(csv_file, delimiter=',')
line_count = 0
for row in csv_reader:
    # At the header line count the number of training choices
    if line_count == 0:
        choice_count = len(row) - 2 - training_index
    else:
        email = row[email_index]
        # ignore duplicate registrations
        if email.lower() in registered_users:
            continue
        registered_users.append(email.lower())

        initial_choices = row[training_index:training_index + choice_count]
        # participants can select less trainings than the max allowed. Cut off at their final choice
        if "" in initial_choices:
            initial_choices = initial_choices[:initial_choices.index("")]
        choices = list(initial_choices)
        # add new trainings to the data-pool
        for choice in choices:
            if choice not in data:
                data[choice] = [list() for _ in range(choice_count)]
        # change choice order if most desired training is already full
        for i in range(len(choices)):
            choices[0], choices[i] = choices[i], choices[0]
            if len(data[choices[0]][0]) < slot_limit:
                break
        else:
            # if all selected trainings are full, the original order will be used for the waiting list
            choices = initial_choices
        for i in range(len(choices)):
            choice = choices[i]
            data[choice][i].append(f"{email}")
    line_count += 1

csv_file.close()

titles = data.keys()
participants = [functools.reduce(lambda acc, v: acc + v, priority_lists, list()) for priority_lists in data.values()]

# add separator after final slot
for participant_list in participants:
    if len(participant_list) > slot_limit:
        participant_list.insert(slot_limit, "---")
longest_list = functools.reduce(lambda acc, v: acc if acc > len(v) else len(v), participants, 0)

with open(f'Sorted_{os.path.basename(filename)}', 'w', encoding='utf8', newline='') as output_file:
    output_file.write('SEP=,\n')
    csv_writer = csv.writer(output_file, delimiter=',')
    csv_writer.writerow(titles)
    output_file.write("\n")
    for i in range(longest_list):
        csv_writer.writerow(list(map(lambda val: '' if len(val) <= i else val[i], participants)))
